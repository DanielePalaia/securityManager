/*
 * Copyright (c) 2018 Pivotal Software, Inc. All Rights Reserved.
 */
package io.pivotal.pde.gemfire.dynamic.security;

import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.apache.geode.cache.Cache;
import org.apache.geode.cache.CacheFactory;
import org.apache.geode.cache.Region;
import org.apache.geode.cache.RegionShortcut;
import org.apache.geode.cache.execute.Execution;
import org.apache.geode.cache.execute.Function;
import org.apache.geode.cache.execute.FunctionService;
import org.apache.geode.cache.execute.ResultCollector;
import org.apache.geode.distributed.DistributedMember;
import org.apache.geode.management.cli.CommandService;
import org.apache.geode.security.AuthenticationFailedException;
import org.apache.geode.security.ResourcePermission;
import org.apache.geode.security.SecurityManager;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

public class DynamicSecurityManager implements SecurityManager {

	static final String SECURITY_PEER_USER = "gfpeer";
	static final String SECURITY_ADMIN_USER = "gfadmin";
	
	static final String SECURITY_ADMIN_PASS_PROP = "security-admin-password";
	static final String SECURITY_PEER_PASS_PROP = "security-peer-password";
	static final String SECURITY_UNAME_PROP = "security-username";
	static final String SECURITY_PASS_PROP = "security-password";
	static final String CLIENT_IP = "security-clientip";
	static final String SECURITY_DISK_STORE_DIR_PROP = "security-disk-store-dir";
	
	static final String USERS_REGION = "_gemusers";

	private static Log log = LogFactory.getLog(DynamicSecurityManager.class);
	
	static Flag bypass = new Flag();  // thread local
	
	private String peerPassword;
	private String adminPassword;
	
	String securityDiskDir;
	
	
	// DynamicSecurityManager has some initialization tasks that can't
	// be done in "init" because at least one data node needs to be up 
	// and running 
	private boolean initialized = false;
	
	// the gotoMember is just a member on which the AuthenticateFunction has already
	// worked (it will work on any data node but not locators) - this avoids the 
	// need to figure this out every time authenticate is called on a locator. 
	// It may be possible to replace this by using FunctionService.onMember(groups ...) 
	// to dispatch the AuthenticateFunction but which group should be used ?
	private DistributedMember gotoMember = null;
	
	
	@Override
	public void init(Properties securityProps) {
		this.peerPassword = securityProps.getProperty(SECURITY_PEER_PASS_PROP);
		this.adminPassword = securityProps.getProperty(SECURITY_ADMIN_PASS_PROP);
		this.securityDiskDir = securityProps.getProperty(SECURITY_DISK_STORE_DIR_PROP);
		
		if (securityDiskDir == null)
			throw new RuntimeException("Could not initialize security manager due to missing required property: " + SECURITY_DISK_STORE_DIR_PROP);
		
		if (this.peerPassword == null)
			throw new RuntimeException("Could not initialize security manager due to missing required property: " + SECURITY_PEER_PASS_PROP);
		
		if (this.adminPassword == null)
			throw new RuntimeException("Could not initialize security manager due to missing required property: " + SECURITY_ADMIN_PASS_PROP);		
	}
	
	private Object authenticateOnMember(DistributedMember m, String uname, String pass){
		Execution exec = FunctionService.onMember(m).setArguments(new String[]{uname, pass});
		Function f = new AuthenticateFunction();
		ResultCollector<Object, List<Object>> results = exec.execute(f);
		List<Object> plist = results.getResult();
		if (plist.size() != 1)
			throw new AuthenticationFailedException("Unexpected behavior: AuthenticateFunction returned " + plist.size() +  " results");
		
		return plist.get(0);
	}

	@Override
	public Object authenticate(Properties props) throws AuthenticationFailedException {
		String uname = props.getProperty(SECURITY_UNAME_PROP);
		String pass = props.getProperty(SECURITY_PASS_PROP);
		String ipAddress = props.getProperty(CLIENT_IP);

		if ( uname.equals(SECURITY_ADMIN_USER) && pass.equals(adminPassword)){

			log.info("User: " + uname + " with privilege level of ADMIN " + " from ip: " + ipAddress + " succesfully logged to the system as admin");
			User u = new GFAdmin();
			u.setUname("gfadmin");
			u.setClientIp(ipAddress);

			return u;
		}
		
		if (uname == null || pass == null)
			throw new AuthenticationFailedException("Authentication failed due to missing credentials");

		if ( uname.equals(SECURITY_PEER_USER) && pass.equals(peerPassword)) {

			log.info("User: " + uname + " with privilege level of PEER: " + " from ip: " + ipAddress + " succesfully logged to the system as peer");

			User u = new GFPeer();
			u.setUname("gfpeer");
			u.setClientIp(ipAddress);
			return u;
		}

		// do the one time initialization here
		//initCluster();

		// well this is a pain in the butt
		// It's all because sometimes this will happen on a locator where 
		// the region isn't present.
		Object result = null;
		Region<String, User> gemusersRegion = CacheFactory.getAnyInstance().getRegion(USERS_REGION);
		if (gemusersRegion != null){
			DynamicSecurityManager.bypass.set(true);
			try {
				User u = gemusersRegion.get(uname);
				u.setUname(uname);
				u.setClientIp(ipAddress);
				if (u == null || ! u.passwordMatches(pass)) {
					writeUserFailedAuthenticationLog(u, uname, ipAddress);
					throw new AuthenticationFailedException("User does not exist or password does not match");
				}
				else
					writeUserAuthenticationLog(u.getLevel(), uname, ipAddress);
				result = u;
			} finally {
				writeUserFailedAuthenticationLog(null, uname, ipAddress);
				DynamicSecurityManager.bypass.set(false);
			}

			return result;  //RETURN
		} else {
			if (gotoMember != null) {
				try {
					result = authenticateOnMember(gotoMember, uname, pass);
					writeUserAuthenticationLog((User.Level) result, uname, ipAddress);

					writeUserAuthenticationLog((User.Level.valueOf(result.toString())), uname, ipAddress);
					User userResult = new User();
					userResult.setLevel((User.Level.valueOf(result.toString())));
					userResult.setClientIp(ipAddress);
					userResult.setUname(uname);
					//log.info("after casting userResult to u the name is: " + u.getUname());
					return userResult; //RETURN
				} catch(Exception  x){
					x.printStackTrace();
				}
			}
			
			// ok, gotoMember was null or did not succeed
			Set<DistributedMember> others = CacheFactory.getAnyInstance().getDistributedSystem().getAllOtherMembers();
			for(DistributedMember m: others){
				try {
					result = authenticateOnMember(m, uname, pass);
					gotoMember = m;
					writeUserAuthenticationLog((User.Level.valueOf(result.toString())), uname, ipAddress);
					User userResult = new User();
					userResult.setLevel((User.Level.valueOf(result.toString())));
					userResult.setClientIp(ipAddress);
					userResult.setUname(uname);
					//log.info("after casting userResult to u the name is: " + u.getUname());
					return userResult; //RETURN
				} catch(Exception x){
					x.printStackTrace();
				}				
			}
		}

		writeUserFailedAuthenticationLog(null, uname, ipAddress);
		throw new AuthenticationFailedException("Something unexpected happened during authentication");
	}

	@Override
	public boolean authorize(Object principal, ResourcePermission permission) {
		if (bypass.get()) return true;
				
		User u = (User) principal;

		Boolean approved = u.canDo(permission);
		String level = getPrivileges(u);

		if(approved)
			log.info("user " + u.getUname() + " with privilege level: " + level + " from ip: " + u.getClientIp() +  " approved to operation " + permission.getOperation().toString() + " on accessing " + permission.getResource() + " on region " + permission.getRegionName());
		else
			log.info("user " + u.getUname() + " with privilege level: " + level + " from ip: " + u.getClientIp() +  " not approved to operation " + permission.getOperation().toString() + " on accessing " + permission.getResource() + " on region " + permission.getRegionName());

		return approved;
	}

	
	private synchronized void initCluster(){
		if (initialized) return;
		
		Cache cache = CacheFactory.getAnyInstance();
		if (cache.isServer()){
			 BootstrapFunction.initCluster(securityDiskDir);
		} else {
			String []args = new String[] { securityDiskDir };
			Execution exec = FunctionService.onMembers().setArguments(args);
			Function bootstrapFunction = new BootstrapFunction();
			ResultCollector<String,List<String>> results = exec.execute(bootstrapFunction);
			List<String> result = results.getResult();
		}
		
		this.initialized = true;
	}

	private void writeUserAuthenticationLog(User.Level level, String uname, String ipAddress)  {
		log.info("entering writeUserAuthenticationLog");
		switch(level) {
			case ADMIN:
				log.info("User: " + uname + " with privilege level " + "ADMIN " +  " from ip: " + ipAddress + " succesfully logged to the system as admin");
				break;
			case WRITER:
				log.info("User: " + uname + " with privilege level " + "WRITER " + " from ip: " + ipAddress + " succesfully logged to the system as writer");
				break;
			case READER:
				log.info("User: " + uname + " with privilege level " + "READER " + " from ip: " + ipAddress + " succesfully logged to the system as reader");
				break;
			case MONITOR:
				log.info("User: " + uname + " with privilege level " + "MONITOR " + " from ip: " + ipAddress + " succesfully logged to the system as monitor");
				break;
		}
		log.info("quitting writeUserAuthenticationLog");

	}

	private void writeUserFailedAuthenticationLog(User u, String uname, String ipAddress)  {
		if(u == null)  {

			log.info("User: " + uname  + " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
			return;
		}
		switch(u.getLevel()) {
			case ADMIN:
				log.info("User: " + uname + " with privilege level " + "ADMIN " +  " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
				break;
			case WRITER:
				log.info("User: " + uname + " with privilege level " + "WRITER " + " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
				break;
			case READER:
				log.info("User: " + uname + " with privilege level " + "READER " + " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
				break;
			case MONITOR:
				log.info("User: " + uname + " with privilege level " + "MONITOR " + " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
				break;
			default:
				log.info("User: " + uname  + " from ip: " + ipAddress + " not logged to the system: WRONG USERNAME AND PASSWORD");
				break;
		}

	}

	private String getPrivileges(User u)  {

		String level = "";

		switch (u.getLevel())  {
			case ADMIN:
				level = "ADMIN";
				break;
			case PEER:
				level = "PEER";
				break;
			case WRITER:
				level = "WRITER";
				break;
			case READER:
				level = "READER";
				break;
			case MONITOR:
				level = "MONITOR";
				break;
		}

		return level;

	}
	
	static class Flag extends ThreadLocal<Boolean> {
		@Override
		protected Boolean initialValue() {
			return Boolean.FALSE;
		}
		
	}
	
	static class ThreadLocalUser extends ThreadLocal<User>{
		
	}
	
}
